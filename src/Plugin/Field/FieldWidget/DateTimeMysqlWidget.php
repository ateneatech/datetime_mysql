<?php

namespace Drupal\datetime_mysql\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDefaultWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'datetime_default' widget.
 *
 * @FieldWidget(
 *   id = "datetime_mysql",
 *   label = @Translation("Date and time -Mysql datetime field-"),
 *   field_types = {
 *     "mysqldatetime"
 *   }
 * )
 */
class DateTimeMysqlWidget extends DateTimeDefaultWidget implements ContainerFactoryPluginInterface {


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    foreach ($items as $delta => $item) {
      if (!$item->date) {
        $items[$delta]->date = new \Drupal\Core\Datetime\DrupalDateTime($item->value);
      }
    }

    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    return $element;
  }

}

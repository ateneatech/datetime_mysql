<?php
/**
 * @file
 * Contains \Drupal\cesefor\Plugin\Field\FieldType\MysqlDatetimeItem
 */

namespace Drupal\datetime_mysql\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Defines the 'datetime' entity field type.
 *
 * @FieldType(
 *   id = "datetime_mysql",
 *   label = @Translation("Datetime Mysql"),
 *   description = @Translation("Create and store date values."),
 *   default_widget = "datetime_mysql",
 *   default_formatter = "datetime_mysql",
 *   list_class = "\Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList"
 * )
 * )
 */
class DatetimeMysqlItem extends DateTimeItem {

  /**
   * {@inheritdoc}
   */

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(t('Date value'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'description' => 'The date value.',
          'type' => 'datetime',
          'mysql_type' => 'datetime',
        ),
      ),
      'indexes' => array(
        'value' => array('value'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */

  public function onChange($property_name, $notify = TRUE) {
    // Notify the parent of changes.
    if ($notify && isset($this->parent)) {
      $this->parent->onChange($this->name);
    }
  }


}